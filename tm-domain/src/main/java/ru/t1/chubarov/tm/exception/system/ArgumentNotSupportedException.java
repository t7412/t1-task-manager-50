package ru.t1.chubarov.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class ArgumentNotSupportedException extends AbstractSystemException {

    public ArgumentNotSupportedException(@NotNull String arg) {
        super("Error. Argument \"" + arg + "\" not support.");
    }
}
