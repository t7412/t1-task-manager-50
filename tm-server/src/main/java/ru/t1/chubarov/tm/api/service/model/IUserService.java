package ru.t1.chubarov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chubarov.tm.enumerated.Role;
import ru.t1.chubarov.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExist(@Nullable String email) throws Exception;

    @Nullable
    User findByLogin(@NotNull String login) throws Exception;

    @Nullable
    User findByEmail(@NotNull String email) throws Exception;

    @NotNull
    User findOneById(@Nullable String id) throws Exception;

    void removeOne(@NotNull User model) throws Exception;

    void removeByLogin(@NotNull String login) throws Exception;

    void removeByEmail(@NotNull String email) throws Exception;

    void setPassword(@Nullable String id, @Nullable String password) throws Exception;

    void updateUser(@Nullable String id,
                       @Nullable String firstName,
                       @Nullable String lastName,
                       @Nullable String middleName) throws Exception;

    void lockUserByLogin(@Nullable String login) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

    @NotNull
    Collection<User> set(@NotNull Collection<User> models) throws Exception;

    void clear() throws Exception;

    @NotNull
    List<User> findAll() throws Exception;

    long getSize() throws Exception;

}
